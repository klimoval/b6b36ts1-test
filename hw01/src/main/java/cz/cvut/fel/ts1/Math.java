package cz.cvut.fel.ts1;

public class Math {

    public static void main(String[] args) {
        System.out.println("result for 3! is " + factorial(3));
    }

    public static long factorial(int num) {
        if (num <= 1) return 1;
        return num * factorial(num - 1);
    }
}
