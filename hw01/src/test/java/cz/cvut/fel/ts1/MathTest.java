package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MathTest {
    @Test
    public void factorialTest() {
        Math math = new Math();
        assertEquals(math.factorial(3), 6);
    }
}
